﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideWall : MonoBehaviour{
    [SerializeField]
    private GameManager gameManager;

    public PlayerControl player;

    // Start is called before the first frame update
    void Start(){
        
    }

    // Update is called once per frame
    void Update(){
        
    }

    // called when ball is colliding with the left or right wall
    void OnTriggerEnter2D(Collider2D anotherCollider){
        if (anotherCollider.name == "Ball"){
            // add score
            player.IncrementScore();

            // restart game if score still < max score
            if (player.Score < gameManager.maxScore)
                anotherCollider.gameObject.SendMessage("RestartGame", 2.0f, SendMessageOptions.RequireReceiver);
        }
    }
}
